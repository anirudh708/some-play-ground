
* Requirements to execute
    * Gradle
    * Java 8

* Running the project:
      
      * cd some-playground/some-playground-server
      * gradle some-playground-wrapper:bootrun
      
* Testing
    *Postman collection  https://www.getpostman.com/collections/d921fc42af724c2cee64
    *http://localhost:30000/swagger-ui.html#/playground-controller                     
