package com.some.playground.server.core.datatypes;

public class Constants {
    public final static String X_REQUESTED_BY = "X_REQUESTED_BY";
    public final static String X_REQUEST_ID = "X_REQUEST_ID";
    public final static String REMOVE = "REMOVE";
    public final static String ADD = "ADD";
    public final static String QUEUED = "QUEUED";

}
