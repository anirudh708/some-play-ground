package com.some.playground.server.core.datatypes.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
public enum Errors {

    PM_INTERNAL_ERROR("INTERNAL_ERROR", 1401, HttpStatus.INTERNAL_SERVER_ERROR, "Internal Error"),
    PM_CONFIGURATION_DONE("PM_CONFIGURATION_DONE", 1402, HttpStatus.BAD_REQUEST, "play ground is configurable once"),
    PM_INCORRECT_CONFIGURATION("PM_INCORRECT_CONFIGURATION", 1403, HttpStatus.BAD_REQUEST, "Incorrect configuration"),
    PM_KID_ALREADY_IN_PLAYSITE("KID_ALREADY_IN_ANOTHER_PLAYSITE", 1404, HttpStatus.BAD_REQUEST, "Kid already in playsite"),
    PM_KID_NOT_IN_PLAYSITE("KID_NOT_IN_PLAYSITE", 1405, HttpStatus.BAD_REQUEST, "Kid not in playsite"),
    PM_PLAYSITE_FULL("PLAYSITE_FULL", 1406, HttpStatus.BAD_REQUEST, "Play site is full please send wait true"),
    PM_KID_NEVER_PLAYED("KID_NEVER_PLAYED", 1407, HttpStatus.BAD_REQUEST, "kid never played"),
    PM_INCORRECT_LOG("INCORRECT_LOG", 1408, HttpStatus.INTERNAL_SERVER_ERROR, "Incorrect log"),
    PM_INCORRECT_DATE("INCORRECT_DATE", 1409, HttpStatus.BAD_REQUEST, "enter a parsable date"),
    PM_SITE_NEVER_UTILIZED("SITE_NEVER_UTLIZED", 1410, HttpStatus.BAD_REQUEST, "site never utilized or not there");


    @Getter
    private String code;
    @Getter
    private int internalErrorCode;
    @Getter
    private HttpStatus status;
    @Getter
    private String description;
}
