package com.some.playground.server.core.datatypes.enums;

public enum PlaySiteType {
    DOUBLE_SWING,
    CAROUSEL,
    SLIDE,
    BALL_PIT;
}
