package com.some.playground.server.core.datatypes.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum SuccessCodes {

    CREATED("CREATED", "Created"),
    SUCCESSFULLY_UPDATED("SUCCESSFULLY_UPDATED", "Updated"),
    SUCCESSFULLY_ADDED("SUCCESSFULLY_ADDED", "Added"),
    DELETED("DELETED","Deleted"),
    OK("OK","Ok");

    @Getter
    private String code;
    @Getter
    private String description;


}
