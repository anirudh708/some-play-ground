package com.some.playground.server.core.datatypes.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.some.playground.server.core.datatypes.response.ErrorResponse;
import com.some.playground.server.core.datatypes.enums.Errors;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.http.HttpStatus;

import java.util.Map;

@Data
@ToString(callSuper = false)
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class PlaygroundServiceException extends Exception {

    private int errorStatusCode;
    private int errorInternalStatusCode;
    private String errorReasonCode;
    private String errorDescription;
    private HttpStatus status;
    private Map<String, String> additionalData;

    public PlaygroundServiceException(int errorStatusCode, int errorInternalStatusCode, String errorReasonCode,
                                      String errorDescription, HttpStatus status) {
        this.errorStatusCode = errorStatusCode;
        this.errorInternalStatusCode = errorInternalStatusCode;
        this.errorReasonCode = errorReasonCode;
        this.errorDescription = errorDescription;
        this.status = status;
    }

    public PlaygroundServiceException(Errors errors, Exception e) {
        super(errors.getDescription());
        this.errorStatusCode = errors.getStatus().value();
        this.errorDescription = errors.getDescription();
        this.errorReasonCode = errors.getCode();
        this.status = errors.getStatus();
        this.errorInternalStatusCode = errors.getInternalErrorCode();
        this.addSuppressed(e);
    }


    public PlaygroundServiceException(Errors errors) {
        super(errors.getDescription());
        this.errorStatusCode = errors.getStatus().value();
        this.errorDescription = errors.getDescription();
        this.errorReasonCode = errors.getCode();
        this.status = errors.getStatus();
        this.errorInternalStatusCode = errors.getInternalErrorCode();
    }

    public PlaygroundServiceException(Errors errors, Map<String, String> additionalData) {
        super(errors.getDescription());
        this.errorStatusCode = errors.getStatus().value();
        this.errorDescription = errors.getDescription();
        this.errorReasonCode = errors.getCode();
        this.status = errors.getStatus();
        this.errorInternalStatusCode = errors.getInternalErrorCode();
        if (additionalData != null)
            this.additionalData = additionalData;
    }

    public PlaygroundServiceException(Errors errors, Map<String, String> additionalData, Exception e) {
        super(errors.getDescription());
        this.addSuppressed(e);
        this.errorStatusCode = errors.getStatus().value();
        this.errorDescription = errors.getDescription();
        this.errorReasonCode = errors.getCode();
        this.status = errors.getStatus();
        this.errorInternalStatusCode = errors.getInternalErrorCode();
        if (additionalData != null)
            this.additionalData = additionalData;
    }

    public PlaygroundServiceException(ErrorResponse errors) {
        this.errorStatusCode = errors.getErrorStatusCode();
        this.errorDescription = errors.getErrorDescription();
        this.errorReasonCode = errors.getErrorReasonCode();
        this.status = HttpStatus.valueOf(errors.getErrorStatusCode() == 0 ? 500 : errors.getErrorStatusCode());
        this.errorInternalStatusCode = errors.getErrorInternalStatusCode();
        if (additionalData != null)
            this.additionalData = errors.getAdditionalData();
    }
}
