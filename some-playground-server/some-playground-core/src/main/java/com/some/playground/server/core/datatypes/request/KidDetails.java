package com.some.playground.server.core.datatypes.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class KidDetails {
    @NotNull
    String name;
    @NotNull
    Integer age;
    @NotNull
    Boolean waits=true;
    @NotNull
    String ticketNumber;
    @NotNull
    Boolean vip;
}
