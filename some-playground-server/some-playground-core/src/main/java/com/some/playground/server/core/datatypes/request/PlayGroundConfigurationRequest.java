package com.some.playground.server.core.datatypes.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.some.playground.server.core.datatypes.enums.PlaySiteType;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Map;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlayGroundConfigurationRequest {
    @NotNull
    Map<PlaySiteType, PlaySiteInputRequest> playSiteCount;
}
