package com.some.playground.server.core.datatypes.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.some.playground.server.core.datatypes.exception.PlaygroundServiceException;
import com.some.playground.server.core.datatypes.enums.Errors;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * Created by anirudh.w.
 */
@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorResponse {

    private int errorStatusCode;
    private int errorInternalStatusCode;
    private String errorReasonCode;
    private String errorDescription;
    private Map<String, String> additionalData;


    public ErrorResponse(PlaygroundServiceException e) {
        this.errorStatusCode = e.getErrorStatusCode();
        this.errorReasonCode = e.getErrorReasonCode();
        this.errorDescription = e.getErrorDescription();
        this.errorInternalStatusCode = e.getErrorInternalStatusCode();
        if (e.getAdditionalData() != null)
            this.additionalData = e.getAdditionalData();
    }

    public ErrorResponse(Errors errors) {
        this.errorStatusCode = errors.getStatus().value();
        this.errorReasonCode = errors.getCode();
        this.errorDescription = errors.getDescription();
        this.errorInternalStatusCode = errors.getInternalErrorCode();
    }
}
