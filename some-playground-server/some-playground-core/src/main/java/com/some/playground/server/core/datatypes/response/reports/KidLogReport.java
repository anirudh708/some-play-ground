package com.some.playground.server.core.datatypes.response.reports;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class KidLogReport {
    Integer numOfTimesPlayed;
    Long totalTimeInSeconds;
    public KidLogReport(Integer times, Long totalTimeInSeconds){
        this.numOfTimesPlayed = times;
        this.totalTimeInSeconds = totalTimeInSeconds;
    }
}
