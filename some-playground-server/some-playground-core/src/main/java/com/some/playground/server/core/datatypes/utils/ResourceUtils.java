package com.some.playground.server.core.datatypes.utils;


import com.some.playground.server.core.datatypes.enums.Errors;
import com.some.playground.server.core.datatypes.exception.PlaygroundServiceException;
import com.some.playground.server.core.datatypes.response.ClientResponse;
import com.some.playground.server.core.datatypes.response.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by anirudh.w.
 */
@Slf4j
public class ResourceUtils {

    public static void logWrappedException(Exception wrappedException) {
        if(wrappedException!=null)
            log.error("wrapped exception : ",wrappedException);
    }

    public static ClientResponse setClientErrorResponse(PlaygroundServiceException e) {
        ClientResponse clientResponse = new ClientResponse();
        clientResponse.setSuccess(false);
        ErrorResponse errorResponse = new ErrorResponse(e);
        clientResponse.setErrorResponse(errorResponse);
        clientResponse.setHttpStatus(HttpStatus.valueOf(errorResponse.getErrorStatusCode()));
        return clientResponse;
    }

    public static ErrorResponse setErrorResponse(Exception e) {
        ErrorResponse errorResponse = new ErrorResponse(Errors.PM_INTERNAL_ERROR);
        Map<String,String> additionalData = new HashMap<>();
        additionalData.put("exception_details", e.getMessage());
        errorResponse.setAdditionalData(additionalData);
        return errorResponse;
    }
}
