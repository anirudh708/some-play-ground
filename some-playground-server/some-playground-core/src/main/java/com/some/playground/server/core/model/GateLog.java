package com.some.playground.server.core.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.some.playground.server.core.datatypes.enums.PlaySiteType;
import lombok.Builder;
import lombok.Getter;
import org.joda.time.DateTime;

@Builder
@Getter
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GateLog {
    private String ticketNumber;
    private PlaySiteType playSite;
    private String operation; //Enum?
    private DateTime logTime;
}
