package com.some.playground.server.core.model;


import com.some.playground.server.core.datatypes.enums.PlaySiteType;
import com.some.playground.server.core.model.handlers.PlaySiteHandler;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
public class PlayGround {
    private Map<PlaySiteType, PlaySiteHandler> playSiteHandlers;
    private boolean configured = false;
}
