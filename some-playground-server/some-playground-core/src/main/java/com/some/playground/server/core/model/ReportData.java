package com.some.playground.server.core.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.some.playground.server.core.datatypes.enums.PlaySiteType;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReportData {
    private Map<String, List<GateLog>> ticketDetails = new HashMap<>();
    private Map<PlaySiteType, List<GateLog>> siteLogDetails = new HashMap<>();
    private Map<PlaySiteType, List<UtilizationLog>> siteUtilizationSnaps = new HashMap<>();
}
