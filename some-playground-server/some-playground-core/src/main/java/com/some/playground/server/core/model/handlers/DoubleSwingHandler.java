package com.some.playground.server.core.model.handlers;

import com.some.playground.server.core.datatypes.enums.PlaySiteType;
import com.some.playground.server.core.datatypes.exception.PlaygroundServiceException;
import com.some.playground.server.core.datatypes.request.KidDetails;
import com.some.playground.server.core.model.PlayGround;
import com.some.playground.server.core.model.ReportData;
import com.some.playground.server.core.model.metadata.DoubleSwingMetaData;
import com.some.playground.server.core.service.api.ReportingService;
import com.some.playground.server.core.utils.HandlerFactory;
import com.sun.org.apache.xpath.internal.operations.Bool;
import lombok.NoArgsConstructor;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

import static com.some.playground.server.core.datatypes.Constants.ADD;
import static com.some.playground.server.core.datatypes.Constants.QUEUED;
import static com.some.playground.server.core.datatypes.Constants.REMOVE;

@NoArgsConstructor
@Component
public class DoubleSwingHandler implements PlaySiteHandler<DoubleSwingMetaData> {
    // Todo check shiftings.
    private DoubleSwingMetaData data;
    private List<KidDetails> waitlist;

    @Autowired
    ReportingService reportingService;


    @Override
    public void initalize(DoubleSwingMetaData data) {
        this.data = data;
        this.waitlist = new ArrayList<>();
    }

    @Override
    public Boolean addKid(KidDetails details) throws PlaygroundServiceException {
        if (isSpacePresent()) {
            addKidAndReport(details);
            return true;
        } else {
            if (details.getWaits()) {
                if(details.getVip()) {
                    int vipIndex = -4;
                    for (int i = (waitlist.size() - 1); i >= 0; i --) {
                        if (waitlist.get(i).getVip()) {
                            vipIndex = i;
                            break;
                        }
                    }
                    int index = (vipIndex+4)>waitlist.size()?waitlist.size():vipIndex+4;
                    waitlist.add(index, details);
                } else {
                    waitlist.add(details);
                }
                reportingService.logKidEvent(QUEUED, details.getTicketNumber(), PlaySiteType.DOUBLE_SWING);
                return true;
            } else {
                return false;
            }
        }
    }

    private boolean isSpacePresent() {
        return data.getKids().size() < data.getTotalSpacesAvailable();
    }

    public Boolean removeKid(String ticketId) throws PlaygroundServiceException{
        if(!removeKidWhilePlayingAndReport(ticketId)) {
            return removeKidFromQueue(ticketId);
        }
        return true;
    }

    private boolean removeKidWhilePlayingAndReport(String ticketId) throws PlaygroundServiceException {
        int index = -1;
        for (int i = 0; i < data.getKids().size(); i++) {
            if (data.getKids().get(i).getTicketNumber().equals(ticketId)) {
                index = i;
                break;
            }
        }
        if (index >= 0) {
            data.getKids().remove(index);
            if (!CollectionUtils.isEmpty(waitlist)) {
                data.getKids().add(waitlist.get(0));
                waitlist.remove(0);
            }
            reportingService.logKidEvent(REMOVE, ticketId, PlaySiteType.DOUBLE_SWING);
            return true;
        }
        return false;
    }

    private boolean removeKidFromQueue(String ticketId) throws PlaygroundServiceException {
        int index = -1;
        for (int i = 0; i < waitlist.size(); i++) {
            if (waitlist.get(i).getTicketNumber().equals(ticketId)) {
                index = i;
                break;
            }
        }
        if (index >= 0) {
            waitlist.remove(index);
            reportingService.logKidEvent(REMOVE, ticketId, PlaySiteType.DOUBLE_SWING);
            return true;
        }
        return false;
    }

    private void addKidAndReport(KidDetails details) throws PlaygroundServiceException {
        data.getKids().add(details);
        reportingService.logKidEvent(ADD, details.getTicketNumber(), PlaySiteType.DOUBLE_SWING);
    }

    @PostConstruct
    @Override
    public void registerHandler() {
        HandlerFactory.registerTaskHandler(this);
    }

    @Override
    public PlaySiteType getHandlerId() {
        return PlaySiteType.DOUBLE_SWING;
    }

    @Override
    public Double getUtilizationPercent() {
        Double value =  ((double)(data.getKids().size()) / data.getTotalSpacesAvailable()) * 100;
        reportingService.logUtilizationSnapshot(PlaySiteType.DOUBLE_SWING, value);
        return value;
    }

}
