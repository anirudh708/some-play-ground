package com.some.playground.server.core.model.handlers;

import com.some.playground.server.core.datatypes.enums.PlaySiteType;
import com.some.playground.server.core.datatypes.exception.PlaygroundServiceException;
import com.some.playground.server.core.datatypes.request.KidDetails;
import com.some.playground.server.core.model.metadata.PlaySiteMetaData;
import javafx.print.PageLayout;

public interface PlaySiteHandler<T extends PlaySiteMetaData> {
    void initalize(T data);
    Boolean addKid(KidDetails details) throws PlaygroundServiceException;
    Boolean removeKid(String ticket) throws PlaygroundServiceException;
    void registerHandler();
    PlaySiteType getHandlerId();
    Double getUtilizationPercent();
}
