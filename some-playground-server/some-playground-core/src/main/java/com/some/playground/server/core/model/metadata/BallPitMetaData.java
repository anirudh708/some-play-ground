package com.some.playground.server.core.model.metadata;

public class BallPitMetaData extends PlaySiteMetaData {
    public BallPitMetaData(int totalSpots, int eachSiteMax) {
        super(totalSpots * eachSiteMax);
    }
}
