package com.some.playground.server.core.model.metadata;

public class CarouselMetaData extends PlaySiteMetaData {
    public CarouselMetaData(int totalSpots, int eachSiteMax) {
        super(totalSpots * eachSiteMax);
    }
}
