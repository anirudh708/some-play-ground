package com.some.playground.server.core.model.metadata;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DoubleSwingMetaData extends PlaySiteMetaData {
    public DoubleSwingMetaData(int totalPlaySites) {
        super(totalPlaySites * 2);
    }
}
