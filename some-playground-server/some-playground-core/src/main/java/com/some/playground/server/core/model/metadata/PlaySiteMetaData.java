package com.some.playground.server.core.model.metadata;

import com.some.playground.server.core.datatypes.request.KidDetails;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public abstract class PlaySiteMetaData {
    Integer totalSpacesAvailable;
    List<KidDetails> kids;
    PlaySiteMetaData(int totalSpacesAvailable){
        this.totalSpacesAvailable = totalSpacesAvailable;
        this.kids = new ArrayList<>();
    }
}
