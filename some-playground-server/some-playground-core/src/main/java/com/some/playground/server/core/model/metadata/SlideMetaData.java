package com.some.playground.server.core.model.metadata;

public class SlideMetaData extends PlaySiteMetaData {
    public SlideMetaData(int totalSpots, int eachSiteMax) {
        super(totalSpots * eachSiteMax);
    }
}
