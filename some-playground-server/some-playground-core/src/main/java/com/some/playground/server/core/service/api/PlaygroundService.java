package com.some.playground.server.core.service.api;

import com.some.playground.server.core.datatypes.enums.PlaySiteType;
import com.some.playground.server.core.datatypes.exception.PlaygroundServiceException;
import com.some.playground.server.core.datatypes.request.KidDetails;
import com.some.playground.server.core.datatypes.request.PlaySiteInputRequest;

import java.util.Map;

public interface PlaygroundService {

    Map<PlaySiteType, PlaySiteInputRequest> configurePlayground(Map<PlaySiteType, PlaySiteInputRequest> initialConfig)
            throws PlaygroundServiceException;

    boolean isConfigured();

    void addKidToPlaysite(PlaySiteType type, KidDetails kid) throws PlaygroundServiceException;

    void removeKidFromCurrentPlaySite(String ticketNum) throws PlaygroundServiceException;

    Map<String, Double> takePlaySiteSnapShot(PlaySiteType playSiteType) throws PlaygroundServiceException;

}
