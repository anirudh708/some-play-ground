package com.some.playground.server.core.service.api;

import com.some.playground.server.core.datatypes.enums.PlaySiteType;
import com.some.playground.server.core.datatypes.exception.PlaygroundServiceException;
import com.some.playground.server.core.datatypes.request.KidDetails;
import com.some.playground.server.core.model.GateLog;
import com.some.playground.server.core.model.PlayGround;
import com.some.playground.server.core.model.UtilizationLog;

import java.util.List;
import java.util.Set;

public interface ReportingService {
    boolean isKidCurrentlyInPlaySite(String ticket) throws PlaygroundServiceException;
    PlaySiteType getKidCurrentPlaySite(String ticket) throws PlaygroundServiceException;
    void logKidEvent(String operation, String ticket, PlaySiteType playSiteType) throws PlaygroundServiceException;
    List<GateLog> getKidLog(String ticketId) throws PlaygroundServiceException;
    List<GateLog> getSiteLog(Set<PlaySiteType> site) throws PlaygroundServiceException;
    void logUtilizationSnapshot(PlaySiteType playSiteType,Double percent);
    List<UtilizationLog> getUtilizationSnapshot(PlaySiteType playSiteType) throws PlaygroundServiceException;
}
