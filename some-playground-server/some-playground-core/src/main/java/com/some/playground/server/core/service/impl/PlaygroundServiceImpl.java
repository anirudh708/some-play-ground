package com.some.playground.server.core.service.impl;

import com.some.playground.server.core.datatypes.enums.Errors;
import com.some.playground.server.core.datatypes.enums.PlaySiteType;
import com.some.playground.server.core.datatypes.exception.PlaygroundServiceException;
import com.some.playground.server.core.datatypes.request.KidDetails;
import com.some.playground.server.core.datatypes.request.PlaySiteInputRequest;
import com.some.playground.server.core.model.handlers.*;
import com.some.playground.server.core.model.PlayGround;
import com.some.playground.server.core.service.api.PlaygroundService;
import com.some.playground.server.core.service.api.ReportingService;
import com.some.playground.server.core.utils.HandlerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static com.some.playground.server.core.utils.Constants.UTILIZATION_PERCENT;
import static com.some.playground.server.core.utils.ObjectUtils.*;

@Component
@SuppressWarnings("unchecked")
public class PlaygroundServiceImpl implements PlaygroundService {

    @Autowired
    private PlayGround playGround;

    @Autowired
    private ReportingService reportingService;

    @Override
    public Map<PlaySiteType, PlaySiteInputRequest> configurePlayground(Map<PlaySiteType, PlaySiteInputRequest> initialConfig)
            throws PlaygroundServiceException {
        Map<PlaySiteType, PlaySiteInputRequest> response = new HashMap();
        if (!isConfigured()) {
            Map<PlaySiteType, PlaySiteHandler> map = new ConcurrentHashMap<>();
            for (PlaySiteType type : initialConfig.keySet()) {
                map.put(type, HandlerFactory.getHandler(type));
                map.get(type).initalize(getPlaySiteMetaData(type, initialConfig.get(type)));
                response.put(type, initialConfig.get(type));
            }
            playGround.setPlaySiteHandlers(map);
            playGround.setConfigured(true);
            return response;
        } else {
            throw new PlaygroundServiceException(Errors.PM_CONFIGURATION_DONE);
        }
    }

    @Override
    public boolean isConfigured() {
        return playGround.isConfigured();
    }

    @Override
    public void addKidToPlaysite(PlaySiteType type, KidDetails kid) throws PlaygroundServiceException {
        if (kidInPlaySite(kid.getTicketNumber())) {
            throw new PlaygroundServiceException(Errors.PM_KID_ALREADY_IN_PLAYSITE);
        }
        PlaySiteHandler handler =getConfiguredPlaySiteHandler(type);

        if(!handler.addKid(kid)){
            throw new PlaygroundServiceException(Errors.PM_PLAYSITE_FULL);
        }
    }

    public void removeKidFromCurrentPlaySite(String ticketNumber) throws PlaygroundServiceException {
        PlaySiteType type = reportingService.getKidCurrentPlaySite(ticketNumber);
        if (type == null) {
            throw new PlaygroundServiceException(Errors.PM_KID_NOT_IN_PLAYSITE);
        }

        PlaySiteHandler handler = getConfiguredPlaySiteHandler(type);

        if (!handler.removeKid(ticketNumber)){
            throw new PlaygroundServiceException(Errors.PM_KID_NOT_IN_PLAYSITE);
        }
    }

    public Map<String, Double> takePlaySiteSnapShot(PlaySiteType playSiteType) throws PlaygroundServiceException {
        PlaySiteHandler handler = getConfiguredPlaySiteHandler(playSiteType);
        Map map = new HashMap<String, Double>();
        map.put(UTILIZATION_PERCENT, handler.getUtilizationPercent());
        return map;
    }

    private PlaySiteHandler getConfiguredPlaySiteHandler(PlaySiteType playSiteType) throws PlaygroundServiceException {
        PlaySiteHandler handler = playGround.getPlaySiteHandlers()
                .get(playSiteType);
        if (handler == null) {
            throw new PlaygroundServiceException(Errors.PM_INCORRECT_CONFIGURATION);
        }
        return handler;
    }

    private boolean kidInPlaySite(String ticket) throws PlaygroundServiceException{
        return reportingService.isKidCurrentlyInPlaySite(ticket);
    }

}
