package com.some.playground.server.core.service.impl;

import com.some.playground.server.core.datatypes.enums.Errors;
import com.some.playground.server.core.datatypes.enums.PlaySiteType;
import com.some.playground.server.core.datatypes.exception.PlaygroundServiceException;
import com.some.playground.server.core.datatypes.request.KidDetails;
import com.some.playground.server.core.model.GateLog;
import com.some.playground.server.core.model.ReportData;
import com.some.playground.server.core.model.UtilizationLog;
import com.some.playground.server.core.service.api.ReportingService;
import com.sun.org.apache.bcel.internal.generic.RETURN;
import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Component
@SuppressWarnings("unchecked")
public class ReportingServiceImpl implements ReportingService {

    @Autowired
    private ReportData reportData;

    @Override
    public boolean isKidCurrentlyInPlaySite(String ticket) throws PlaygroundServiceException {
        return getKidCurrentPlaySite(ticket) != null;
    }

    @Override
    public PlaySiteType getKidCurrentPlaySite(String ticket) throws PlaygroundServiceException {
        List<GateLog> kidLog = reportData.getTicketDetails().get(ticket);

        if (CollectionUtils.isEmpty(kidLog)) {
            return null;
        }
        String op = kidLog.get(kidLog.size() - 1).getOperation();

        if (op.equals("ADD") || op.equals("QUEUED")) {
            return kidLog.get(kidLog.size() - 1).getPlaySite();
        }

        return null;
    }

    @Override
    public void logKidEvent(String operation, String ticket, PlaySiteType playSiteType) throws PlaygroundServiceException {
        GateLog gl = GateLog.builder()
                .logTime(new DateTime())
                .operation(operation)
                .playSite(playSiteType)
                .ticketNumber(ticket)
                .build();

        reportData.getTicketDetails().computeIfAbsent(ticket, k -> new ArrayList<>());
        reportData.getTicketDetails().get(ticket).add(gl);
        reportData.getSiteLogDetails().computeIfAbsent(playSiteType, k -> new ArrayList<>());
        reportData.getSiteLogDetails().get(playSiteType).add(gl);
    }

    public List<GateLog> getKidLog(String ticketId) throws PlaygroundServiceException {
        if (CollectionUtils.isEmpty(reportData.getTicketDetails().get(ticketId))) {
            throw new PlaygroundServiceException(Errors.PM_KID_NEVER_PLAYED);
        }
        return reportData.getTicketDetails().get(ticketId);
    }

    public List<GateLog> getSiteLog(Set<PlaySiteType> site) throws PlaygroundServiceException {
        List<GateLog> gateLogs = new ArrayList<>();
        if (CollectionUtils.isEmpty(site)) {
            reportData.getSiteLogDetails().values().forEach(gateLogs::addAll);
        } else {
            reportData.getSiteLogDetails().keySet().stream().filter(k -> site.contains(k))
                    .forEach( k -> gateLogs.addAll(reportData.getSiteLogDetails().get(k)));
        }
        if (CollectionUtils.isEmpty(gateLogs)) {
            throw new PlaygroundServiceException(Errors.PM_KID_NEVER_PLAYED);
        }
        return gateLogs;
    }

    public void logUtilizationSnapshot(PlaySiteType playSiteType,Double percent) {
        UtilizationLog log = UtilizationLog.builder()
                .percent(percent)
                .playSiteType(playSiteType)
                .dateTime(new DateTime())
                .build();
        reportData.getSiteUtilizationSnaps().computeIfAbsent(playSiteType, k -> new ArrayList<>());
        reportData.getSiteUtilizationSnaps().get(playSiteType).add(log);
    }
    public List<UtilizationLog> getUtilizationSnapshot(PlaySiteType playSiteType) throws PlaygroundServiceException {
        if (CollectionUtils.isEmpty(reportData.getSiteUtilizationSnaps().get(playSiteType))) {
            throw new PlaygroundServiceException(Errors.PM_SITE_NEVER_UTILIZED);
        }
        return reportData.getSiteUtilizationSnaps().get(playSiteType);
    }


}
