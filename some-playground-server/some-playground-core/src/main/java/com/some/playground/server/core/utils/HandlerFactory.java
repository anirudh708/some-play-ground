package com.some.playground.server.core.utils;

import com.some.playground.server.core.datatypes.enums.PlaySiteType;
import com.some.playground.server.core.model.handlers.PlaySiteHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class HandlerFactory {

    private static Map<PlaySiteType, PlaySiteHandler> taskHandlerMapping = new HashMap<>();

    public static <T extends PlaySiteHandler> void registerTaskHandler (T handler) {
        if (taskHandlerMapping == null)
            taskHandlerMapping = new ConcurrentHashMap<>();
        taskHandlerMapping.put(handler.getHandlerId(), handler);
    }
    public static PlaySiteHandler getHandler (PlaySiteType handler) {
        return taskHandlerMapping.get(handler);
    }
}
