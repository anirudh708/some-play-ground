package com.some.playground.server.core.utils;

import com.some.playground.server.core.datatypes.enums.Errors;
import com.some.playground.server.core.datatypes.enums.PlaySiteType;
import com.some.playground.server.core.datatypes.exception.PlaygroundServiceException;
import com.some.playground.server.core.datatypes.request.PlaySiteInputRequest;
import com.some.playground.server.core.model.PlayGround;
import com.some.playground.server.core.model.ReportData;
import com.some.playground.server.core.model.handlers.*;
import com.some.playground.server.core.model.metadata.*;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Slf4j
@Configuration
@Component("objectUtils")
public class ObjectUtils {
    @Bean
    public PlayGround getSinglePlayGroundInstance() {
        return new PlayGround();
    }

    @Bean
    public ReportData getSingleReportDataInstance() {
        return new ReportData();
    }


    public static PlaySiteMetaData getPlaySiteMetaData(PlaySiteType type, PlaySiteInputRequest details) throws PlaygroundServiceException{
        switch (type) {
            case DOUBLE_SWING:
                validateData(details.getTotalSites(), 2);
                return new DoubleSwingMetaData(details.getTotalSites());
            case SLIDE:
                validateData(details.getTotalSites(), details.getMaxKidsPerSite());
                return new SlideMetaData(details.getTotalSites(), details.getMaxKidsPerSite());
            case BALL_PIT:
                validateData(details.getTotalSites(), details.getMaxKidsPerSite());
                return new BallPitMetaData(details.getTotalSites(), details.getMaxKidsPerSite());
            case CAROUSEL:
                validateData(details.getTotalSites(), details.getMaxKidsPerSite());
                return new CarouselMetaData(details.getTotalSites(), details.getMaxKidsPerSite());
            default:
                throw new PlaygroundServiceException(Errors.PM_INCORRECT_CONFIGURATION);
        }
    }

    private static void validateData(Integer totalSites, Integer maxKids) throws PlaygroundServiceException {
        if(totalSites == null || maxKids == null) {
            throw new PlaygroundServiceException(Errors.PM_CONFIGURATION_DONE);
        }
    }
}
