package com.some.playground.server.wrapper.controller;

import com.codahale.metrics.annotation.Metered;
import com.codahale.metrics.annotation.Timed;
import com.some.playground.server.core.datatypes.Constants;
import com.some.playground.server.core.datatypes.enums.PlaySiteType;
import com.some.playground.server.core.datatypes.exception.PlaygroundServiceException;
import com.some.playground.server.core.datatypes.request.KidDetails;
import com.some.playground.server.core.datatypes.request.PlayGroundConfigurationRequest;
import com.some.playground.server.core.datatypes.response.ClientResponse;
import com.some.playground.server.core.datatypes.utils.ResourceUtils;
import com.some.playground.server.wrapper.service.api.PlaygroundWrapperService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by anirudh.w.
 */

@RestController
@RequestMapping("/playground/")
@Slf4j
@Api(value = "/", description = "Container APIs")
public class PlaygroundController {

    @Autowired
    private PlaygroundWrapperService playgroundWrapperService;


    @RequestMapping(value = "/configure", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Metered(name = "configurePlayGround", absolute = true)
    public
    @ResponseBody
    ResponseEntity configurePlayGround(@RequestHeader(value = Constants.X_REQUESTED_BY)
                                        @ApiParam(value = Constants.X_REQUESTED_BY)
                                                String requestedBy,
                                        @RequestHeader(value = Constants.X_REQUEST_ID)
                                        @ApiParam(value = Constants.X_REQUEST_ID) String requestId,
                                       @RequestBody @ApiParam(value = "play ground initial config request",
                                               required = true)
                                       @Valid PlayGroundConfigurationRequest request) {
        //TODO: Add a method to reconfigure.
        ClientResponse clientResponse = null;
        try {
            // Request ID can be used for Idempotance in Future.
            clientResponse = playgroundWrapperService.setPlayGroundConfiguration(request);
        } catch (PlaygroundServiceException e) {
            clientResponse = ResourceUtils.setClientErrorResponse(e);
        }
        return new ResponseEntity<>(clientResponse, clientResponse.getHttpStatus());
    }

    @RequestMapping(value = "{playsite}/add-kid", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Metered(name = "addKidtoPlaySite", absolute = true)
    public
    @ResponseBody
    ResponseEntity addKidtoPlaySite(@RequestHeader(value = Constants.X_REQUESTED_BY)
                                       @ApiParam(value = Constants.X_REQUESTED_BY)
                                               String requestedBy,
                                       @RequestHeader(value = Constants.X_REQUEST_ID)
                                       @ApiParam(value = Constants.X_REQUEST_ID) String requestId,
                                       @PathVariable(value = "playsite")
                                       @ApiParam(name = "playsite", required = true) PlaySiteType playSite,
                                       @RequestBody @ApiParam(value = "play ground initial config request",
                                            required = true)
                                       @Valid KidDetails kid) {
        //TODO: Add a method to reconfigure.
        ClientResponse clientResponse = null;
        try {
            clientResponse = playgroundWrapperService.addKidToPlaySite(playSite, kid);
        } catch (PlaygroundServiceException e) {
            clientResponse = ResourceUtils.setClientErrorResponse(e);
        }
        return new ResponseEntity<>(clientResponse, clientResponse.getHttpStatus());
    }

    @RequestMapping(value = "/remove-kid/{ticket}", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Metered(name = "removeKidPlaySite", absolute = true)
    public
    @ResponseBody
    ResponseEntity removeKidFromCurrentPlaySite(@RequestHeader(value = Constants.X_REQUESTED_BY)
                                    @ApiParam(value = Constants.X_REQUESTED_BY)
                                            String requestedBy,
                                    @RequestHeader(value = Constants.X_REQUEST_ID)
                                    @ApiParam(value = Constants.X_REQUEST_ID) String requestId,
                                    @PathVariable(value = "ticket")
                                    @ApiParam(name = "ticket", required = true) String ticketNumber) {
        ClientResponse clientResponse = null;
        try {
            clientResponse = playgroundWrapperService.removeKidFromCurrentPlaySite(ticketNumber);
        } catch (PlaygroundServiceException e) {
            clientResponse = ResourceUtils.setClientErrorResponse(e);
        }
        return new ResponseEntity<>(clientResponse, clientResponse.getHttpStatus());
    }

    @RequestMapping(value = "/reports/ticket/{ticket}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Metered(name = "getKidHistory", absolute = true)
    public
    @ResponseBody
    ResponseEntity getKidHistory(@RequestHeader(value = Constants.X_REQUESTED_BY)
                                                @ApiParam(value = Constants.X_REQUESTED_BY)
                                                        String requestedBy,
                                                @RequestHeader(value = Constants.X_REQUEST_ID)
                                                @ApiParam(value = Constants.X_REQUEST_ID) String requestId,
                                                @PathVariable(value = "ticket")
                                                @ApiParam(name = "ticket", required = true) String ticketNumber) {
        ClientResponse clientResponse = null;
        try {
            clientResponse = playgroundWrapperService.getKidReport(ticketNumber);
        } catch (PlaygroundServiceException e) {
            clientResponse = ResourceUtils.setClientErrorResponse(e);
        }
        return new ResponseEntity<>(clientResponse, clientResponse.getHttpStatus());
    }

    @RequestMapping(value = "/reports/playsite/count", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Metered(name = "getVisitorCount", absolute = true)
    public
    @ResponseBody
    ResponseEntity getVisitorCount(@RequestHeader(value = Constants.X_REQUESTED_BY)
                                 @ApiParam(value = Constants.X_REQUESTED_BY)
                                         String requestedBy,
                                 @RequestHeader(value = Constants.X_REQUEST_ID)
                                 @ApiParam(value = Constants.X_REQUEST_ID) String requestId,
                                 @RequestParam(value = "date") String date) {
        ClientResponse clientResponse = null;
        try {
            clientResponse = playgroundWrapperService.getTotalVisitorCount(date);
        } catch (PlaygroundServiceException e) {
            clientResponse = ResourceUtils.setClientErrorResponse(e);
        }
        return new ResponseEntity<>(clientResponse, clientResponse.getHttpStatus());
    }

    @RequestMapping(value = "/ticket/{ticket}/history/chain", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Metered(name = "getKidHistoryChain", absolute = true)
    public
    @ResponseBody
    ResponseEntity getKidHistoryChain(@RequestHeader(value = Constants.X_REQUESTED_BY)
                                   @ApiParam(value = Constants.X_REQUESTED_BY)
                                           String requestedBy,
                                   @RequestHeader(value = Constants.X_REQUEST_ID)
                                   @ApiParam(value = Constants.X_REQUEST_ID) String requestId,
                                   @PathVariable(value = "ticket")
                                   @ApiParam(name = "ticket") String ticket) {
        ClientResponse clientResponse = null;
        try {
            clientResponse = playgroundWrapperService.getKidHistoryChain(ticket);
        } catch (PlaygroundServiceException e) {
            clientResponse = ResourceUtils.setClientErrorResponse(e);
        }
        return new ResponseEntity<>(clientResponse, clientResponse.getHttpStatus());
    }

    @RequestMapping(value = "/playsite/{playsite}/utlization", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Metered(name = "getPlaySiteUtilization", absolute = true)
    public
    @ResponseBody
    ResponseEntity getPlaySiteUtilization(@RequestHeader(value = Constants.X_REQUESTED_BY)
                                      @ApiParam(value = Constants.X_REQUESTED_BY)
                                              String requestedBy,
                                      @RequestHeader(value = Constants.X_REQUEST_ID)
                                      @ApiParam(value = Constants.X_REQUEST_ID) String requestId,
                                      @PathVariable(value = "playsite")
                                      @ApiParam(name = "playsite", required = true) PlaySiteType playSite) {
        ClientResponse clientResponse = null;
        try {
            clientResponse = playgroundWrapperService.takePlaySiteSnapShot(playSite);
        } catch (PlaygroundServiceException e) {
            clientResponse = ResourceUtils.setClientErrorResponse(e);
        }
        return new ResponseEntity<>(clientResponse, clientResponse.getHttpStatus());
    }

    @RequestMapping(value = "/playsite/{playsite}/utlization/history", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Metered(name = "getPlaySiteUtilizationHistory", absolute = true)
    public
    @ResponseBody
    ResponseEntity getPlaySiteUtilizationHistory(@RequestHeader(value = Constants.X_REQUESTED_BY)
                                          @ApiParam(value = Constants.X_REQUESTED_BY)
                                                  String requestedBy,
                                          @RequestHeader(value = Constants.X_REQUEST_ID)
                                          @ApiParam(value = Constants.X_REQUEST_ID) String requestId,
                                          @PathVariable(value = "playsite")
                                          @ApiParam(name = "playsite", required = true) PlaySiteType playSite) {
        ClientResponse clientResponse = null;
        try {
            clientResponse = playgroundWrapperService.getPlaySiteUtilizationHistory(playSite);
        } catch (PlaygroundServiceException e) {
            clientResponse = ResourceUtils.setClientErrorResponse(e);
        }
        return new ResponseEntity<>(clientResponse, clientResponse.getHttpStatus());
    }
}
