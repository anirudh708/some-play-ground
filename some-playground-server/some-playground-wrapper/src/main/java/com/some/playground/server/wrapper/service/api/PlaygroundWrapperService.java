package com.some.playground.server.wrapper.service.api;


import com.some.playground.server.core.datatypes.enums.PlaySiteType;
import com.some.playground.server.core.datatypes.exception.PlaygroundServiceException;
import com.some.playground.server.core.datatypes.request.KidDetails;
import com.some.playground.server.core.datatypes.request.PlayGroundConfigurationRequest;
import com.some.playground.server.core.datatypes.response.ClientResponse;

import java.util.Map;

public interface PlaygroundWrapperService {
    ClientResponse<Map<PlaySiteType,Integer>> setPlayGroundConfiguration(PlayGroundConfigurationRequest request) throws PlaygroundServiceException;
    ClientResponse<KidDetails> addKidToPlaySite(PlaySiteType type, KidDetails kid) throws PlaygroundServiceException;
    ClientResponse removeKidFromCurrentPlaySite(String ticket) throws PlaygroundServiceException;
    ClientResponse getKidReport(String ticket) throws PlaygroundServiceException;
    ClientResponse getTotalVisitorCount(String day) throws PlaygroundServiceException;
    ClientResponse getKidHistoryChain(String ticket) throws PlaygroundServiceException;
    ClientResponse takePlaySiteSnapShot(PlaySiteType playSiteType) throws PlaygroundServiceException;
    ClientResponse getPlaySiteUtilizationHistory(PlaySiteType playSiteType) throws PlaygroundServiceException;
}
