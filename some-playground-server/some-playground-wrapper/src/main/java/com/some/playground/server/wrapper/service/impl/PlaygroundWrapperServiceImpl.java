package com.some.playground.server.wrapper.service.impl;

import com.some.playground.server.core.datatypes.Constants;
import com.some.playground.server.core.datatypes.enums.Errors;
import com.some.playground.server.core.datatypes.enums.PlaySiteType;
import com.some.playground.server.core.datatypes.exception.PlaygroundServiceException;
import com.some.playground.server.core.datatypes.request.KidDetails;
import com.some.playground.server.core.datatypes.request.PlayGroundConfigurationRequest;
import com.some.playground.server.core.datatypes.response.ClientResponse;
import com.some.playground.server.core.datatypes.enums.SuccessCodes;
import com.some.playground.server.core.datatypes.response.reports.KidLogReport;
import com.some.playground.server.core.model.GateLog;
import com.some.playground.server.core.model.metadata.PlaySiteMetaData;
import com.some.playground.server.core.service.api.PlaygroundService;
import com.some.playground.server.core.service.api.ReportingService;
import com.some.playground.server.wrapper.service.api.PlaygroundWrapperService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Minutes;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.NoUniqueBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

import static com.some.playground.server.core.datatypes.Constants.ADD;
import static com.some.playground.server.core.datatypes.Constants.QUEUED;
import static com.some.playground.server.core.datatypes.Constants.REMOVE;

@Component
@Slf4j
@SuppressWarnings("unchecked")
public class PlaygroundWrapperServiceImpl implements PlaygroundWrapperService {

    @Autowired
    PlaygroundService playgroundService;

    @Autowired
    ReportingService reportingService;

    @Override
    public ClientResponse<Map<PlaySiteType, Integer>> setPlayGroundConfiguration(PlayGroundConfigurationRequest request) throws PlaygroundServiceException {
        Map response = new HashMap();
        try {
            if (request.getPlaySiteCount().isEmpty()) {
                throw new PlaygroundServiceException(Errors.PM_INCORRECT_CONFIGURATION);
            }
            response = playgroundService.configurePlayground(request.getPlaySiteCount());
        } catch (PlaygroundServiceException e) {
            throw e;
        }
        return new ClientResponse<>(SuccessCodes.CREATED, response);
    }

    @Override
    public ClientResponse<KidDetails> addKidToPlaySite(PlaySiteType type, KidDetails kid) throws PlaygroundServiceException {
        validatePlayGround();
        playgroundService.addKidToPlaysite(type, kid);
        return new ClientResponse<>(SuccessCodes.CREATED, kid);
    }

    public ClientResponse removeKidFromCurrentPlaySite(String ticketNumber) throws PlaygroundServiceException{
        validatePlayGround();
        playgroundService.removeKidFromCurrentPlaySite(ticketNumber);
        return new ClientResponse<>(SuccessCodes.SUCCESSFULLY_UPDATED);
    }

    public ClientResponse getKidReport(String ticket) throws PlaygroundServiceException {
        List<GateLog> kidLog = reportingService.getKidLog(ticket);
        Map<PlaySiteType, KidLogReport> report = new HashMap<>();
        String prevEvent = QUEUED;
        DateTime prevEventTime = kidLog.get(0).getLogTime();
        for (GateLog log : kidLog) {
            report.putIfAbsent(log.getPlaySite(), new KidLogReport(0, 0L));
            KidLogReport rep = report.get(log.getPlaySite());
            if (log.getOperation().equals(ADD)){
                if ((prevEvent.equals(ADD))) {
                    throw new PlaygroundServiceException(Errors.PM_INCORRECT_LOG);
                }
                rep.setNumOfTimesPlayed(rep.getNumOfTimesPlayed() + 1);
            }

            if (log.getOperation().equals(REMOVE)) {
                if (!(prevEvent.equals(ADD)||prevEvent.equals(QUEUED))) {
                    throw new PlaygroundServiceException(Errors.PM_INCORRECT_LOG);
                }
                if (prevEvent.equals(ADD)) {
                    rep.setTotalTimeInSeconds(rep.getTotalTimeInSeconds() +
                            Seconds.secondsBetween(prevEventTime,log.getLogTime()).getSeconds());
                }
            }
            prevEvent = log.getOperation();
            prevEventTime = log.getLogTime();
        }
        return new ClientResponse<>(SuccessCodes.OK, report);
    }

    public ClientResponse getTotalVisitorCount(String day) throws PlaygroundServiceException {
        Date date = null;
        try {
            if (StringUtils.isNotBlank(day))
                date = getDateTime(day);
        } catch (Exception e) {
            throw new PlaygroundServiceException(Errors.PM_INCORRECT_DATE);
        }
        List<GateLog> logs = reportingService.getSiteLog(null);
        Map<PlaySiteType, Integer> count = new HashMap<>();
        for (GateLog log: logs) {
            if (ADD.equals(log.getOperation())) {
                if (StringUtils.isNotBlank(day) || (log.getLogTime().toDate().equals(date))) {
                    count.putIfAbsent(log.getPlaySite(), 0);
                    count.put(log.getPlaySite(), count.get(log.getPlaySite()) + 1);
                }
            }
        }
        return new ClientResponse(SuccessCodes.OK, count);
    }

    public ClientResponse getKidHistoryChain(String ticket) throws PlaygroundServiceException {
        List response = reportingService.getKidLog(ticket);
        Collections.reverse(response);
        return new ClientResponse(SuccessCodes.OK, response);
    }

    public ClientResponse takePlaySiteSnapShot(PlaySiteType playSiteType) throws PlaygroundServiceException {
        return new ClientResponse(SuccessCodes.OK, playgroundService.takePlaySiteSnapShot(playSiteType));
    }

    public ClientResponse getPlaySiteUtilizationHistory(PlaySiteType playSiteType) throws PlaygroundServiceException {
        return new ClientResponse(SuccessCodes.OK, reportingService.getUtilizationSnapshot(playSiteType));
    }

    private Date getDateTime(String day)
    {
        DateTimeFormatter format = DateTimeFormat.forPattern("yyyyMMdd");
        LocalDate d = format.parseLocalDate(day);
        return  d.toDate();
    }

    private void validatePlayGround() throws PlaygroundServiceException{
        if (!playgroundService.isConfigured())
            throw new PlaygroundServiceException(Errors.PM_INCORRECT_CONFIGURATION);
    }
}
